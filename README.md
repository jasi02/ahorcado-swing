# Ahorcado Swing

Trabajo práctico N°1 de programación 3

El objetivo del trabajo práctico es implementar una aplicación para jugar al juego clásico del
“ahorcado”. La aplicación selecciona una palabra de su lista de palabras, y el usuario debe
adivinar la palabra arriesgando letras una por una. Cada vez que el usuario propone una
letra que forma parte de la palabra, se deben mostrar todas las apariciones de la letra en la
palabra. El usuario tiene una cierta cantidad de intentos fallidos, luego de los cuales pierde
el juego.
Prueba de subida
